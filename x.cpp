

// Illustrate using CreateThread in gthreads

// George F. Riley, Georgia Tech, ECE3090, Spring 2012


#include "gthread.h"   // Must be included to use the gthreads library
void BubbleSort(int* d, int startingPoint, int length)
{ // This is the thread starting point
// This is where, in this example, the sorting of array d will be done
EndThread();  // Call this just before exiting
}

const int nThreads = 4;      // Number of threads desired
const int maxSize = 512000;  // Largest sort size
int main()
{
int d[maxSize];           // Array to be sorted
int start = 0;            // Starting point of sub-array
int lengthPerThread = maxSize / nThreads;   // Length of sub-array
for (int k = 0; k < nThreads; ++k)
{ // Create each of the four sorting threads
CreateThread(BubbleSort, d, start, lengthPerThread);
start = start + lengthPerThread;
}
// At this point all threads are created

WaitAllThreads();  // This waits until all child threads are done
// Perform the merge procedure to merge the separate sub-arrays
}
