// Parallel Numerical Integration
// ECE2893 Assignment 7, Spring 2011
// YOUR NAME HERE

#include <stdio.h>
#include <math.h>
#include "gthread.h"

// The typedef below defines whether we are using 32-bit float
// or 64-bit double for all of the floating point values.  Your program
// should use this type (MyFloat_t) for all variables that use floating
// point.
typedef MYFLOAT  MyFloat_t;

// The below is the "correct" value, determined by evaluating the
// integral and computing the area.  This is used when computing
// the "error" for the numerical integration calculations.
// Also, this is the only place in the program where the type "double"
// is to be used.  Everywhere else, you should use type MyFloat_t.
double correct = 5.9763555230000e+08;

// The below global variable is used by the threads to update the
// total area of the integral.  You will need mutex protection before
// updating this variable.
MyFloat_t totalArea;
gthread_mutex_t myLock=0; //setting mutex
const int nThreads=10;	//Number of Threads desired

void numericalintegrator (MyFloat_t strt,MyFloat_t lngth,MyFloat_t del)
{
	StartThread();
	MyFloat_t x=strt;
	x=x+(del/2);
	for (int i=0; i<lngth; i++)
	{
		MyFloat_t y= 23.864*x+5.7*x*x-173.1*x+10.534;
		MyFloat_t a= del*y;
		x=x+del;
		LockMutex(myLock);
		totalArea= totalArea+a;
		UnlockMutex(myLock);
	}
	EndThread();
	
}


int main()
{
  // Print out whether using float or double
  if (sizeof(MyFloat_t) == 4)
    {
      printf("Using float\n");
    }
  else
    {
      printf("Using double\n");
    }
  // Your code here.  You need to loop 7 times with 7 different
  // deltaX values, as described in the assignment.  For each of
  // the 7 iterations, create 10 threads and compute the integral
  // numerically using the Riemann sum metthod.  After all 10 threads
  // have completed, print out the deltaX value, the computed integral
  // and the error.
for (int p=0;p<7;p++)
{
	MyFloat_t power= pow(10, p);
	MyFloat_t deltaX= 1/power;
	MyFloat_t start=0;
	MyFloat_t lengthThread= power/nThreads;
	
	for (int k=0; k< nThreads; k++)
	{
		CreateThread(numericalintegrator, start, lengthThread, deltaX);
		start = start + lengthThread;
	}
	WaitAllThreads();
	MyFloat_t error=(correct - totalArea);
	if (error<0){
		error= -error;
	}
	printf("DeltaX %3.6f , Computer Integral %15.13e , Error %15.13e", deltaX, totalArea, error);
	totalArea=0;
}
}




  
